<?php
namespace Excellence\CustomField\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;

class CustomFieldObserver implements ObserverInterface
{
    protected $checkoutSession;

    public function __construct(CheckoutSession $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(EventObserver $observer)
    {
        $quote = $this->checkoutSession->getQuote();
        $order = $observer->getOrder();
        $order->setCustomField($quote->getCustomField());
        return $this;
    }
}
