<?php
namespace Excellence\CustomField\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'custom_field',
            [
                'nullable' => false,
                'length' => 255,
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Custom Field',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'custom_field',
            [
                'nullable' => false,
                'length' => 255,
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Custom Field',
            ]
        );
        $installer->endSetup();
    }
}
