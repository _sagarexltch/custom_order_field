/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/place-order',
    'mage/url'
], function ($, quote, urlBuilder, customer, placeOrderService, mageUrlBuilder) {
    'use strict';

    return function (paymentData, messageContainer) {
        var serviceUrl, payload;
        $.ajax({
            url: mageUrlBuilder.build('customfield/index/custom'),
            type: 'POST',
            dataType: 'json',
            data: {
                customdata: jQuery('[name="custom_field"]').val(),
            },
            complete: function (response) {
                payload = {
                    cartId: quote.getQuoteId(),
                    billingAddress: quote.billingAddress(),
                    paymentMethod: paymentData
                };

                if (customer.isLoggedIn()) {
                    serviceUrl = urlBuilder.createUrl('/carts/mine/payment-information', {});
                } else {
                    serviceUrl = urlBuilder.createUrl('/guest-carts/:quoteId/payment-information', {
                        quoteId: quote.getQuoteId()
                    });
                    payload.email = quote.guestEmail;
                }

                return placeOrderService(serviceUrl, payload, messageContainer);
                console.log('sucsess');
            },
            error: function (xhr, status, errorThrown) {
                console.log('Error happens. Try again.');
            }
        });
    };
});