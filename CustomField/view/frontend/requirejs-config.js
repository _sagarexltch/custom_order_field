var config = {
    map: {
        '*': {
            'Magento_Checkout/template/payment.html':
                'Excellence_CustomField/template/payment.html',
            'Magento_Checkout/js/action/place-order': 'Excellence_CustomField/js/action/place-order'
        }
    }
};