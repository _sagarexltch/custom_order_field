<?php

namespace Excellence\CustomField\Controller\Index;

class Custom extends \Magento\Framework\App\Action\Action
{

    protected $quoteRepository;
    protected $cartFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\QuoteRepositoryFactory $quoteRepository,
        \Magento\Checkout\Model\Cart $cartFactory
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->cartFactory = $cartFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $cartId = $this->cartFactory->getQuote()->getId();
        $quoteRepository = $this->quoteRepository->create();
        $customfield = $this->getRequest()->getPostValue('customdata');
        $quote = $quoteRepository->getActive($cartId);
        $quote->setCustomField($customfield);
        $quote->save();
    }
}
